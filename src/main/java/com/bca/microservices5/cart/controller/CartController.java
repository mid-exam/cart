package com.bca.microservices5.cart.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bca.microservices5.cart.entity.Product;
import com.bca.microservices5.cart.io.response.ListCartDetailResponse;
import com.bca.microservices5.cart.io.response.ListOrderProductResponse;
import com.bca.microservices5.cart.service.CartService;

@RestController
@RequestMapping("/cart")
public class CartController {
	
	@Autowired
	private CartService cartService;

	@GetMapping("/product/")
	public ListCartDetailResponse inquiryStage(@RequestParam String user_id){
		ListCartDetailResponse response = new ListCartDetailResponse(); 
		
		response.setMessage("Berhasil menampilkan data");
		response.setResponse_code("00");
		response.setCartDetail(cartService.getListProductOrdered(user_id));
		
		return response;
	}
}
