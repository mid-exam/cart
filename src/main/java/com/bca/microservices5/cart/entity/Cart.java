package com.bca.microservices5.cart.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data @Entity
@Table(name = "cart")
public class Cart {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id_cart")
	private int id;
	
	@Column (name = "user_id",unique = true)
	private String userid;
}
