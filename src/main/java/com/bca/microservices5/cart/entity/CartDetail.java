package com.bca.microservices5.cart.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Entity @Data
@Table (name = "cart_detail")
public class CartDetail {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name="id_detail")
	private int iddetail;
	
	@Column (name="id_cart")
	private int idcart;
	
	@Column (name = "id_product")
	private int idproduct;
	
	@Transient
	private Product product;
	
	@Column (name = "qty")
	private int qty;
	
	@Column (name = "price")
	private int price;
	
	@Column (name = "status")
	private String status;
	
}
