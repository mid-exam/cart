package com.bca.microservices5.cart.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity @Data
@Table(name = "product")
public class Product {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "nama_barang")
	private String nama;
	
	@Column(name = "harga")
	private int harga;
	
	@Column(name = "brand")
	private String brand;
	
	@Column(name = "kategori")
	private String kategori;
	
	@Column(name = "sub_kategori")
	private String subkategori;
	
}
