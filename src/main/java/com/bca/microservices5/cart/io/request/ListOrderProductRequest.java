package com.bca.microservices5.cart.io.request;

import java.util.List;

import lombok.Data;

@Data
public class ListOrderProductRequest {
	
	private List<Integer> listProductId;

}
