package com.bca.microservices5.cart.io.response;

import java.util.List;

import com.bca.microservices5.cart.entity.CartDetail;

import lombok.Data;

@Data
public class ListCartDetailResponse {

	private String message;
	private String response_code;
	private List<CartDetail> cartDetail;
}
