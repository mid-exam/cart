package com.bca.microservices5.cart.io.response;

import java.util.List;

import com.bca.microservices5.cart.entity.Product;

import lombok.Data;

@Data
public class ListOrderProductResponse {
	
	private List<Product> products;

}
