package com.bca.microservices5.cart.repo;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.bca.microservices5.cart.entity.CartDetail;

public interface CartDetailRepo extends PagingAndSortingRepository<CartDetail, Integer> {

	List<CartDetail> findByIdcartAndStatus(int idCart,String status);
}
