package com.bca.microservices5.cart.repo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.bca.microservices5.cart.entity.Cart;

public interface CartRepo extends PagingAndSortingRepository<Cart, Integer> {

	Cart findByUserid(String userId);
	
}
