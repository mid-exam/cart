package com.bca.microservices5.cart.repo;

import java.util.List;

import org.springframework.boot.test.autoconfigure.properties.PropertyMapping;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Value;

import com.bca.microservices5.cart.entity.Product;
import com.bca.microservices5.cart.io.request.ListOrderProductRequest;
import com.bca.microservices5.cart.io.response.ListOrderProductResponse;

@Repository
@PropertyMapping(value = "application.properties")
public class ProductRepo {
	
	@Value("${product.list.uri}")
	String url;

	public List<Product> productList(List<Integer> listProductId) {

		RestTemplate restTemp = new RestTemplate();
		ResponseEntity<ListOrderProductResponse> listproductCallResp = null;
		ListOrderProductRequest prodReq = new ListOrderProductRequest();
		
		prodReq.setListProductId(listProductId);

		try {
			listproductCallResp = restTemp.postForEntity(url, prodReq, ListOrderProductResponse.class);
			
			ListOrderProductResponse product = listproductCallResp.getBody();

			return product.getProducts();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("FAILED");
			return null;
		} finally {

		}
	}

}
