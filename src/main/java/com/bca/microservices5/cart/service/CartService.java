package com.bca.microservices5.cart.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bca.microservices5.cart.entity.Cart;
import com.bca.microservices5.cart.entity.CartDetail;
import com.bca.microservices5.cart.entity.Product;
import com.bca.microservices5.cart.repo.CartDetailRepo;
import com.bca.microservices5.cart.repo.CartRepo;
import com.bca.microservices5.cart.repo.ProductRepo;

@Service
public class CartService {
	
	@Autowired
	private ProductRepo productRepo;
	@Autowired
	private CartRepo cartRepo;
	@Autowired
	private CartDetailRepo cartDtRepo;
	
	public List<CartDetail> getListProductOrdered(String user_id){
		
		List<CartDetail> cartDt = new ArrayList<CartDetail>();
		
		Cart cart = cartRepo.findByUserid(user_id);
		cartDt = cartDtRepo.findByIdcartAndStatus(cart.getId(), "Ordered");
		
		List<Integer> prodIds = cartDt.stream().map(x -> x.getIdproduct()).collect(Collectors.toList());
		
		List<Product> prodList = productRepo.productList(prodIds);
		
		cartDt = cartDt.stream().map(temp->{
			temp.setProduct(prodList.stream().filter(x->x.getId()==temp.getIdproduct()).findAny().orElse(null));
			return temp;
		}).collect(Collectors.toList());
		
		return cartDt;
	}

}
