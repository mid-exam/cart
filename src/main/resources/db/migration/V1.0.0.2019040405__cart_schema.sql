create table cart (
	id_cart int AUTO_INCREMENT,
	user_id varchar(15),
	primary key (id_cart)
);

create table cart_detail (
  id_detail int AUTO_INCREMENT,
  id_cart int,
  id_product int,
  qty int,
  price int,
  status varchar(20),
  primary key (id_detail),
  foreign key (id_cart) references cart(id_cart)
);



